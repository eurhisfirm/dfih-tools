require("dotenv").config();

import { validateConfig } from "./validators/config";

const config = {
  database: {
    connectString:
      process.env.DATABASE_CONNECT_STRING || "ORACLE_SERVER_DOMAIN_NAME/dbdfih",
    user: process.env.DATABASE_USERNAME || "DATABASE_USERNAME",
    password: process.env.DATABASE_PASSWORD || "DATABASE_PASSWORD"
  }
};

const [validConfig, error] = validateConfig(config);
if (error !== null) {
  console.error(
    `Error in configuration:\n${JSON.stringify(
      validConfig,
      null,
      2
    )}\nError:\n${JSON.stringify(error, null, 2)}`
  );
  process.exit(-1);
}

export default validConfig;
