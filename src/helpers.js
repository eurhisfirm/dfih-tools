import originalSlugify from "slug";

const slugifyCharmap = {
  ...originalSlugify.defaults.charmap,
  "'": " ",
  "@": " ",
  ".": " "
};

export function cleanUpLine(text) {
  text = text.trim();
  if (text.startsWith('"') && text.endsWith('"')) {
    text = text.substring(1, text.length - 1).trim();
  }
  return text.split(/\s+/).join(" ");
}

export function objectsFromSqlResult({ metaData, rows }) {
  return rows.map(row => {
    return metaData
      .map((keyInfo, index) => [keyInfo.name, row[index]])
      .reduce((accumulator, [key, value]) => {
        accumulator[key.toLowerCase()] = value;
        return accumulator;
      }, {});
  });
}

export function slugify(string, replacement) {
  const options = {
    charmap: slugifyCharmap,
    mode: "rfc3986"
  };
  if (replacement) {
    options.replacement = replacement;
  }
  return originalSlugify(string, options);
}

export function truncate(text, length) {
  if (text.length > length) {
    text = text.substring(0, length - 1) + "…";
  }
  return text;
}
