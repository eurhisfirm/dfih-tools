import { validateNonEmptyTrimmedString, validateUrl } from "@biryani/core";

export function validateConfig(config) {
  if (config === null || config === undefined) {
    return [config, "Missing config"];
  }
  if (typeof config !== "object") {
    return [config, `Expected an object got "${typeof config}"`];
  }

  config = { ...config };
  const errors = {};
  const remainingKeys = new Set(Object.keys(config));

  {
    const key = "database";
    if (remainingKeys.delete(key)) {
      const [value, error] = validateDatabase(config[key]);
      config[key] = value;
      if (error !== null) {
        errors[key] = error;
      }
    } else {
      errors[key] = "Missing item";
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item";
  }
  return [config, Object.keys(errors).length === 0 ? null : errors];
}

function validateDatabase(database) {
  if (database === null || database === undefined) {
    return [database, "Missing value"];
  }
  if (typeof database !== "object") {
    return [database, `Expected an object got "${typeof database}"`];
  }

  database = { ...database };
  const errors = {};
  const remainingKeys = new Set(Object.keys(database));

  for (let key of ["connectString", "password", "user"]) {
    if (remainingKeys.delete(key)) {
      const [value, error] = validateNonEmptyTrimmedString(database[key]);
      database[key] = value;
      if (error !== null) {
        errors[key] = error;
      }
    } else {
      errors[key] = "Missing item";
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item";
  }
  return [database, Object.keys(errors).length === 0 ? null : errors];
}
