import assert from "assert";
import commandLineArgs from "command-line-args";
import csvParse from "csv-parse";
import csvStringify from "csv-stringify";
import fs from "fs";
import FuzzySet from "fuzzyset.js";
import jaroWinkler from "jaro-winkler";

import config from "../config";
import { createPool } from "../database";
import { objectsFromSqlResult, slugify } from "../helpers";

const optionDefinitions = [
  {
    name: "names",
    type: String,
    defaultOption: true,
    help: "name of file containing the names of the corporations"
  }
];
const options = commandLineArgs(optionDefinitions);

async function main(pool) {
  const namesCsv = fs.readFileSync(options.names);
  const namesRecords = await new Promise((resolve, reject) => {
    csvParse(
      namesCsv,
      // {
      //   delimiter: ";",
      // },
      function(err, records) {
        if (err) {
          reject(err);
        } else {
          resolve(records);
        }
      }
    );
  });
  const namesHeader = namesRecords.shift();

  const connection = await pool.getConnection();
  try {
    const corporationsIdsByName = {};
    const corporationNamesById = {};

    {
      const entries = objectsFromSqlResult(
        await connection.execute(
          `
            select
              cn.name,
              cn.corporation as id
            from corporation_name cn
          `
        )
      );
      for (let { id, name } of entries) {
        let ids = corporationsIdsByName[name];
        if (ids === undefined) {
          ids = corporationsIdsByName[name] = new Set();
        }
        ids.add(id);

        let names = corporationNamesById[id];
        if (names === undefined) {
          names = corporationNamesById[id] = new Set();
        }
        names.add(name);
      }
    }

    {
      const entries = objectsFromSqlResult(
        await connection.execute(
          `
            select
              ctn.truename as name,
              ctn.corporation as id
            from corporation_true_name ctn
          `
        )
      );
      for (let { id, name } of entries) {
        let ids = corporationsIdsByName[name];
        if (ids === undefined) {
          ids = corporationsIdsByName[name] = new Set();
        }
        ids.add(id);

        let names = corporationNamesById[id];
        if (names === undefined) {
          names = corporationNamesById[id] = new Set();
        }
        names.add(name);
      }
    }

    const corporationsIdsBySlug = {};
    const corporationsSlugsFuzzySet = FuzzySet();
    for (let [name, nameIds] of Object.entries(corporationsIdsByName)) {
      const slug = slugify(name);
      let slugIds = corporationsIdsBySlug[slug];
      if (slugIds === undefined) {
        slugIds = corporationsIdsBySlug[slug] = new Set();
      }
      for (let id of nameIds) {
        slugIds.add(id);
      }

      corporationsSlugsFuzzySet.add(slug);
    }

    const jaroWinklerScoreByIdByName = {};
    const ngramScoreByIdByName = {};
    for (let nameRecord of namesRecords) {
      assert.strictEqual(
        nameRecord.length,
        1,
        `Unexpected name record: ${nameRecord}`
      );
      const name = nameRecord[0];
      const slug = slugify(name);

      const scoreById = (jaroWinklerScoreByIdByName[name] = {});
      for (let [candidateSlug, candidateIds] of Object.entries(
        corporationsIdsBySlug
      )) {
        const score = jaroWinkler(candidateSlug, slug);
        if (score >= 0.9) {
          for (let id of [...candidateIds]) {
            if (scoreById[id] === undefined) {
              scoreById[id] = score;
            }
          }
        }
      }

      const ngramMatches = (corporationsSlugsFuzzySet.get(slug) || []).filter(
        match => match[0] >= 0.8
      );
      if (ngramMatches.length > 0) {
        const scoreById = (ngramScoreByIdByName[name] = {});
        for (let [score, foundSlug] of ngramMatches) {
          for (let id of [...corporationsIdsBySlug[foundSlug]]) {
            if (scoreById[id] === undefined) {
              scoreById[id] = score;
            }
          }
        }
      }
    }

    const associations = [
      [
        "FAVRE_NAME",
        "DFIH_NAME",
        "CORPORATION",
        "JARO_WRINKLER_SCORE",
        "NGRAM_SCORE"
      ]
    ];
    const foundNames = [
      ...new Set([
        ...Object.keys(jaroWinklerScoreByIdByName),
        ...Object.keys(ngramScoreByIdByName)
      ])
    ].sort();
    for (let name of foundNames) {
      const jaroWinklerScoreById = jaroWinklerScoreByIdByName[name] || {};
      const ngramScoreById = ngramScoreByIdByName[name] || {};
      const ids = new Set([
        ...Object.keys(jaroWinklerScoreById),
        ...Object.keys(ngramScoreById)
      ]);
      const scoreById = {};
      for (let id of [...ids]) {
        scoreById[id] = Math.max(
          jaroWinklerScoreById[id] || 0,
          ngramScoreById[id] || 0
        );
      }
      for (let [id, score] of Object.entries(scoreById).sort(
        (entry0, entry1) => entry1[1] - entry0[1]
      )) {
        const corporationNames = [...corporationNamesById[id]];
        const jaroWinklerScore = jaroWinklerScoreById[id];
        const ngramScore = ngramScoreById[id];
        let nearestCorporationName = null;
        if (
          ngramScore === undefined ||
          (jaroWinklerScore !== undefined && jaroWinklerScore >= ngramScore)
        ) {
          let bestName = null;
          let bestScore = -1;
          for (let corporationName of corporationNames) {
            const score = jaroWinkler(corporationName, name);
            if (score > bestScore) {
              bestName = corporationName;
              bestScore = score;
            }
          }
          nearestCorporationName = bestName || corporationNames[0];
        } else {
          const corporationNamesFuzzySet = FuzzySet();
          for (let corporationName of corporationNames) {
            corporationNamesFuzzySet.add(corporationName);
          }
          const ngramMatches = corporationNamesFuzzySet.get(name);
          nearestCorporationName =
            ngramMatches === null ? corporationNames[0] : ngramMatches[0][1];
        }
        associations.push([
          name,
          nearestCorporationName,
          id,
          jaroWinklerScore || "",
          ngramScore || ""
        ]);
      }
    }
    const associationsCsv = await new Promise((resolve, reject) => {
      csvStringify(
        associations,
        // {
        //   delimiter: ";",
        // },
        function(err, csv) {
          if (err) {
            reject(err);
          } else {
            resolve(csv);
          }
        }
      );
    });
    console.log(associationsCsv);
  } finally {
    connection.close();
  }
}

createPool()
  .then(pool => {
    main(pool).catch(error => {
      console.log(error.stack || error);
      process.exit(1);
    });
  })
  .catch(error => {
    console.log(error.stack || error);
    process.exit(1);
  });
