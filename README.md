# DFIH-Tools

_Various tools for the "Données financières historiques (DFIH)" project_

## Install

```bash
git clone https://gitlab.huma-num.fr/eurhisfirm/dfih-tools.git
cd dfih-tools/
```

Edit `src/config.js` to change database informations. Then

```bash
npm install
```

## Associate a list of names to DFIH corporations

Associate using SQL:
```sql
select tf.name, cn.name, cn.corporation, utl_match.jaro_winkler_similarity(lower(tf.name), lower(cn.name)) as score_match
from tmp_favre tf
join corporation_name cn on (1=1)
where  utl_match.jaro_winkler_similarity(lower(tf.name), lower(cn.name)) > 90
order by tf.name, utl_match.jaro_winkler_similarity(lower(tf.name), lower(cn.name)) desc
```

```bash
node_modules/.bin/babel-node src/scripts/associate_names_to_corporations.js data/banks_names.csv > data/associations_of_names_with_banks.csv
```
